import os
from pika import BlockingConnection
from pika.credentials import PlainCredentials
from pika.connection import ConnectionParameters

credentials = PlainCredentials(
    os.environ.get('RABBITMQ_DEFAULT_USER', 'admin'),
    os.environ.get('RABBITMQ_DEFAULT_PASS', 'admin123')
)
connection_parameters = ConnectionParameters(
    host=os.environ.get('RABBITMQ_HOST', '0.0.0.0'),
    credentials=credentials
)

connection = BlockingConnection(connection_parameters)
channel = connection.channel()
queue = os.environ.get('RABBITMQ_QUEUE', 'mediawatchdog')
channel.queue_declare(
    queue=queue
)


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)


channel.basic_consume(callback,
                      queue=queue,
                      no_ack=True)

channel.start_consuming()
