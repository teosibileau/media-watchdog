import os
import time
import json
import logging
from watchdog.observers import Observer

from logstash_async.handler import AsynchronousLogstashHandler
from watchdog.events import FileSystemEventHandler
from pika import BlockingConnection
from pika.credentials import PlainCredentials
from pika.connection import ConnectionParameters


class WatchdogMediaHandler(FileSystemEventHandler):
    def __init__(self, *args, **kwargs):
        super(WatchdogMediaHandler, self).__init__(*args, **kwargs)
        # Logstash Logger
        self.logger = logging.getLogger('python-logstash-logger')
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(AsynchronousLogstashHandler(
            os.environ.get('LOGSTASH_HOST', '0.0.0.0'),
            int(os.environ.get('LOGSTASH_PORT', 5000)),
            database_path='logstash.db'
        ))
        credentials = PlainCredentials(
            os.environ.get('RABBITMQ_DEFAULT_USER', 'admin'),
            os.environ.get('RABBITMQ_DEFAULT_PASS', 'admin123')
        )
        connection_parameters = ConnectionParameters(
            host=os.environ.get('RABBITMQ_HOST', '0.0.0.0'),
            credentials=credentials
        )
        self.connection = BlockingConnection(connection_parameters)
        self.channel = self.connection.channel()
        self.queue = os.environ.get('RABBITMQ_QUEUE', 'mediawatchdog')
        self.channel.queue_declare(
            queue=self.queue
        )

    def broadcast(self, message):
        self.channel.basic_publish(
            exchange='',
            routing_key=self.queue,
            body=json.dumps(message)
        )

    def on_any_event(self, event):
        extra = {
            'event_type': event.event_type,
            'path': event.src_path
        }
        message = 'File:%s Event: %s' % (event.src_path, event.event_type)
        # Log
        self.logger.info(message, extra=extra)
        # Broadcast
        extra['message'] = message
        self.broadcast(extra)


class WatchdogMediaObserver(Observer):
    def unschedule_all(self):
        for watch in self._handlers.keys():
            handler = self._handlers[watch]
            if isinstance(handler, WatchdogMediaHandler):
                handler.connection.close()
        super(WatchdogMediaObserver, self).unschedule_all()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = os.environ.get('MEDIA_WATCHDOG_FOLDER', False)
    if path:
        event_handler = WatchdogMediaHandler()
        observer = WatchdogMediaObserver()
        observer.schedule(
            event_handler,
            path,
            recursive=True
        )
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()
    else:
        print('Please set the MEDIA_WATCHDOG_FOLDER environment variable')
